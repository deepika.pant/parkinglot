package test;
import main.Car;
import main.ParkingLot;
import main.Vehicle;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ParkingLotTest {

    private final ByteArrayOutputStream out = new ByteArrayOutputStream();
    private final ByteArrayOutputStream err = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    public static boolean containsIgnoreCase(String str, String subString) {
           return str.toLowerCase().contains(subString.toLowerCase());
    }

    @Before
    public void setStreams() {
        System.setOut(new PrintStream(out));
        System.setErr(new PrintStream(err));
    }

    @After
    public void restoreInitialStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }


    @Test
    public void testCreateParkingLot() throws Exception {
        ParkingLot instance = new ParkingLot(20);
       assertEquals("20slotsCreated", out.toString().trim().replace(" ", ""));
    }

    @Test
    public void testParkingLotStatus() throws Exception {
        ParkingLot instance = new ParkingLot(3);
        Vehicle car1 = new Car("1234", "Hyundi", "Red");
        Vehicle car2 = new Car("1609", "Maruti", "Blue");
        instance.parkVehicle(car1);
        instance.parkVehicle(car2);
        assertEquals(1, instance.getSlots().size());

    }

    @Test
    public void testCapacity() throws Exception {
        ParkingLot instance = new ParkingLot(20);
        Vehicle car1 = new Car("1234", "Hyundi", "Red");
        Vehicle car2 = new Car("1609", "Maruti", "Blue");
        instance.parkVehicle(car1);
        instance.parkVehicle(car2);
        assertEquals(18, instance.getSlots().size());
        //instance.Clean();
    }

    @Test
    public void testFullParkingLot() throws Exception {
        ParkingLot instance = new ParkingLot(2);
        Vehicle car1 = new Car("1234", "Hyundi", "Red");
        // car2 has license number 1609 and the company name is Google
        Vehicle car2 = new Car("1609", "Maruti", "Blue");
        Vehicle car3 = new Car("1809", "Fiat", "Red");
        instance.parkVehicle(car1);
        instance.parkVehicle(car2);
        instance.parkVehicle(car3);
        //   assertEquals("PARKINGISFULL", out.toString().trim().replace(" ",""));
        assertTrue(containsIgnoreCase(out.toString().trim().replace(" ", ""), "PARKINGISFULL"));
         }

    @Test
    public void testUnParking() throws Exception {
        ParkingLot instance = new ParkingLot(3);
        Vehicle car1 = new Car("1234", "Hyundi", "Red");
        // car2 has license number 1609 and the company name is Google
        Vehicle car2 = new Car("1609", "Maruti", "Blue");
        Vehicle car3 = new Car("1809", "Fiat", "Red");
        instance.parkVehicle(car1);
        instance.parkVehicle(car2);
        instance.leave(car1);
        instance.parkVehicle(car3);
        assertTrue(containsIgnoreCase(out.toString().trim().replace(" ", ""), "SlotAssignedtoVehicle"));

    }

    @Test
    public void testGetTicketNumberForRegNumber() throws Exception {
        ParkingLot instance = new ParkingLot(3);
        Vehicle car1 = new Car("1234", "Hyundi", "Red");
        // car2 has license number 1609 and the company name is Google
        Vehicle car2 = new Car("1609", "Maruti", "Blue");
        Vehicle car3 = new Car("1809", "Fiat", "Red");
        instance.parkVehicle(car1);
        instance.parkVehicle(car2);
        instance.leave(car1);
        instance.parkVehicle(car3);
        instance.getTicketNumberForRegNumber("1609");
        assertTrue(containsIgnoreCase(out.toString().trim().replace(" ", ""),
                "1"));
        instance.getTicketNumberForRegNumber("1809");
        assertTrue(containsIgnoreCase(out.toString().trim().replace(" ", ""),
                "0"));

    }

    @Test
    public void testGetRegistrationNoByColor() throws Exception {
        ParkingLot instance = new ParkingLot(3);
        Vehicle car1 = new Car("1234", "Hyundi", "Red");
        // car2 has license number 1609 and the company name is Google
        Vehicle car2 = new Car("1609", "Maruti", "Blue");
        Vehicle car3 = new Car("1809", "Fiat", "Red");
        instance.parkVehicle(car1);
        instance.parkVehicle(car2);
        instance.parkVehicle(car3);
        instance.getRegistrationOfSameColourCars("Red");
        //The vehicles of  Colour  Red	Reg Number 1234	Reg Number 1809
        assertTrue(containsIgnoreCase(out.toString().trim().replace(" ", ""), "RegNumber1234RegNumber1809"));

    }

    @Test
    public void testGetTicketNumberOfSameColourCars() throws Exception {
        ParkingLot instance = new ParkingLot(3);
        Vehicle car1 = new Car("1234", "Hyundi", "Red");
        // car2 has license number 1609 and the company name is Google
        Vehicle car2 = new Car("1609", "Maruti", "Blue");
        Vehicle car3 = new Car("1809", "Fiat", "Red");
        instance.parkVehicle(car1);
        instance.parkVehicle(car2);
        instance.parkVehicle(car3);
        instance.getTicketNumberOfSameColourCars("Red");
       assertTrue(containsIgnoreCase(out.toString().trim().replace(" ", ""), "ThevehiclesofColourRed:TicketNumber02"));
    }

}