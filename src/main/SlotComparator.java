package main;
import java.util.Comparator;

public class SlotComparator implements Comparator<ParkingSlot> {

    // Overriding compare()method of Comparator
    // for descending order of cgpa
    public int compare(ParkingSlot s1, ParkingSlot s2) {
        if (s1.getDistance() > s2.getDistance())
            return 1;
        else if (s1.getDistance() < s2.getDistance())
            return -1;
        return 0;
    }
}

