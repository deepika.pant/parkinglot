package main;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class ParkingLot {

    // key contains the Colour  name and   vehicles present in the list
    Map<String, ArrayList<Vehicle>> sameColourCar;
    //To get ticket  number for a Reg NUmber
    Map<String, Vehicle> regNumTicketNumMap;
    //Slots associated with Parking Lot
    private PriorityQueue<ParkingSlot> slots;
    //Total number of slots
    private int numberSlots;
    public ParkingLot() {
    }
    // constructor of the class
    public ParkingLot(int numberSlots) {
        this.numberSlots = numberSlots;
        this.sameColourCar = new HashMap<String, ArrayList<Vehicle>>();
        regNumTicketNumMap = new HashMap<String, Vehicle>();
        slots = new PriorityQueue<ParkingSlot>(numberSlots, new SlotComparator());
        for (int i = 0; i < numberSlots; i++) {
            slots.add(new ParkingSlot(i, i + 100));
        }
        System.out.println(numberSlots + "slots Created");
    }

    public int getNumberSlots() {
        return numberSlots;
    }

    public PriorityQueue<ParkingSlot> getSlots() {
        return slots;
    }

    // a method for handling the scenario when a vehicle is parked
    public boolean parkVehicle(Vehicle vehicle) {
        System.out.println(" ----------Entered parkVehicle -----------");
        if (slots.isEmpty()) {
            System.out.println("PARKING IS FULL");
            return false;
        }
        System.out.println("Available Slots" + slots.size());
        while (!slots.isEmpty()) {
            ParkingSlot slot = slots.poll();
            System.out.println("**********" + slot);
            slot.park(vehicle);
            vehicle.parkInSlot(slot);
            //Generate a Ticket
            Ticket ticket = new Ticket(slot.getSlotNumber(), vehicle);
            vehicle.assignTicket(ticket);
            System.out.println(" Slot Assigned to Vehicle--------" + vehicle.companyName + "===Slot Number" + slot.getSlotNumber() + "distance from entrance==" + slot.getDistance());
            if (this.sameColourCar.containsKey(vehicle.colour)) {
                ArrayList<Vehicle> regNoList = this.sameColourCar.get(vehicle.colour);
                this.sameColourCar.remove(vehicle.colour);
                regNoList.add(vehicle);
                this.sameColourCar.put(vehicle.colour, regNoList);
            } else {
                ArrayList<Vehicle> regNoList = new ArrayList<Vehicle>();
                regNoList.add(vehicle);
                this.sameColourCar.put(vehicle.colour, regNoList);
            }
            regNumTicketNumMap.put(vehicle.getLicPlate(), vehicle);
            return true;

        }
        System.out.println("sorry,parkinglotisfull");
        return false;

    }

    // method for handling the scenario when a vehicle leaves
    public boolean leave(Vehicle vehicle) {
        System.out.println(" -------------Entering leave----------------------------- ");
        vehicle.getParkingSlot().unPark();
        vehicle.clearSlots();
        vehicle.clearTicket();
        AddFreeSlotToLot(vehicle.parkingSlot);
        System.out.println("Slot freed  and exited  " + vehicle.licPlate + " of " + vehicle.companyName);
        //Removing from collection
        ArrayList<Vehicle> vhList = this.sameColourCar.get(vehicle.colour);
        // check if the vehicle is present in the list or not
        // if present, remove the vehicle from the list
        vhList.remove(vehicle.getLicPlate());
        //Remove from Map
        regNumTicketNumMap.remove(vehicle.getLicPlate());
        return true;
    }

    // method for displaying the list of vehicle of the given colour
    public void getRegistrationOfSameColourCars(String colour) {
        System.out.println(" ------------------------------------------ ");
        ArrayList<Vehicle> vhList = this.sameColourCar.get(colour);
        if (vhList == null) {
            System.out.println("no car with colour " + colour);
            return;
        }
        System.out.print("The vehicles of  Colour  " + colour + " ");
        for (Vehicle vl : vhList) {
            System.out.print("Reg Number " + vl.getLicPlate() + " ");
        }

    }

    // method for Getting ticket number for a Registration number
    public int getTicketNumberForRegNumber(String RegNumber) {
        if (regNumTicketNumMap.get(RegNumber) != null) {
            System.out.println("Car Parked with registration number" + RegNumber + "Ticket Number" + regNumTicketNumMap.get(RegNumber).getTicket().getNumber());
            return regNumTicketNumMap.get(RegNumber).getTicket().getNumber();

        } else {
            System.out.println("No Car Parked with registration number" + RegNumber);
        }
        return -1;

    }

    // method for displaying the Ticket number of the same  Colour Car
    public void getTicketNumberOfSameColourCars(String colour) {
        System.out.println(" ------------------------------------------ ");
        ArrayList<Vehicle> vhList = this.sameColourCar.get(colour);
        if (vhList == null) {
            System.out.println("no car with colour" + colour + "Parked");
            return;
        }
        System.out.print("The vehicles of  Colour" + colour + " : Ticket Number");
        for (Vehicle vl : vhList) {
            System.out.print(vl.getTicket().getNumber() + " ");
        }

    }

    public void AddFreeSlotToLot(ParkingSlot parkingSlot) {
        slots.add(parkingSlot);
        System.out.println("Available Slots in the Parking lot :" + slots.size());
    }
}