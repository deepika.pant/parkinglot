package main;
public class ParkingSlot {

    private Vehicle vehicle;

    private int slotNumber;
    private int distance;

    // constructor of the class
    public ParkingSlot(int slotNumber, int distance) {
        this.slotNumber = slotNumber;
        this.distance = distance;

    }

    public int getDistance() {
        return distance;
    }
    // private Level l;

    public void park(Vehicle c) {
        this.vehicle = vehicle;
    }

    public void unPark() {
        this.vehicle = null;
    }

    // get method for slot number
    public int getSlotNumber() {
        return slotNumber;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

}
