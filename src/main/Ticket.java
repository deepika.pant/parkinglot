package main;
public class Ticket {

    private int number;
    private Vehicle vehicle;

    public Ticket(int number, Vehicle vehicle) {
        this.number = number;
        this.vehicle = vehicle;

    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
