package main;
public abstract class Vehicle {

    // licence plate of the vehicle
    protected String licPlate;
    protected String companyName;
    protected String colour;
    //Parking slot associated with Vehicle
    ParkingSlot parkingSlot = null;
    private Ticket ticket;

    public ParkingSlot getParkingSlot() {
        return parkingSlot;
    }

    public void setParkingSlot(ParkingSlot parkingSlot) {
        this.parkingSlot = parkingSlot;
    }
    // field of storing the company details from where the vehicle has come

    public String getLicPlate() {
        return licPlate;
    }

    public void setLicPlate(String licPlate) {
        this.licPlate = licPlate;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    // the method adds the filled slot to the parkingSlots list
    public void parkInSlot(ParkingSlot slot) {
        parkingSlot = slot;

    }

    public Ticket getTicket() {
        return ticket;
    }

    public void assignTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public void clearTicket() {
        this.ticket = null;
    }

    public void clearSlots() {
        parkingSlot.unPark();

    }

    public void AssignTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public String toString() {
        return "Registration Number " + licPlate + "CompanyName " + companyName + "colour" + colour;
    }


}