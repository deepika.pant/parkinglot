package main;
public class Car extends Vehicle {
    // constructor of the class Car
    public Car(String licPlate, String companyName, String colour) {
        this.licPlate = licPlate;
        this.companyName = companyName;
        this.colour = colour;
    }

}