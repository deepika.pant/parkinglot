package main;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class main {
    static ParkingLot pl = null;
    static Map<String, Vehicle> carParked = new HashMap<String, Vehicle>();

    public static void main(String[] args) {
        //declare a scanner object to read the command line input by user
        Scanner sn = new Scanner(System.in);
        //loop the utility in loop until the user makes the choice to exit
        while (true) {
            //Print the options for the user to choose from
            System.out.println("*****Available Options*****");
            System.out.println("*. Press 1 for Creating Parking lot");
            System.out.println("*. Press 2 for Parking a car");
            System.out.println("*. Press 3 to Registration numbers of all cars of a particular Color");
            System.out.println("*. Press 4 Ticket number in which a car with a given registration number is placed");
            System.out.println("*. Press 5 Ticket number in which a car with a given color number is placed");
            // Prompt the use to make a choice
            System.out.println("Enter your choice:");
            //Capture the user input in scanner object and store it in a pre decalred variable
            String userInput = sn.next();
            //Check the user input
            switch (userInput) {
                case "1":
                    System.out.println("Enter Number Of Slots:");
                    int noOfLots = sn.nextInt();
                    createLot(noOfLots);
                    System.out.println("Created Parking Lot with Slots" + noOfLots);
                    break;
                case "2":
                    System.out.println("Enter Enter Reg Number of Car");
                    String regNumber = sn.next();
                    System.out.println("Enter Enter CompanyName of Car");
                    String company = sn.next();
                    System.out.println("Enter Enter Colour of Car");
                    String colour = sn.next();
                    //do the job number 2
                    Car car = new Car(regNumber, company, colour);
                    pl.parkVehicle(car);
                    break;
                case "3":
                    System.out.println("Please Enter Colour");
                    colour = sn.next();
                    pl.getRegistrationOfSameColourCars(colour);
                    break;
                case "4":
                    System.out.println("Please Enter Reg Number");
                    regNumber = sn.next();
                    //inform user in case of invalid choice.
                    pl.getTicketNumberForRegNumber(regNumber);
                    break;
                case "5":
                    System.out.println("Please Enter Colour");
                    colour = sn.next();
                    pl.getTicketNumberOfSameColourCars(colour);
                    //inform user in case of invalid choice.
                    break;
                default:
                    //inform user in case of invalid choice.
                    System.out.println("Invalid choice. Read the options carefully");
            }
        }
    }

    public static void createLot(int noOfLots) {
        pl = new ParkingLot(noOfLots);

    }

}
